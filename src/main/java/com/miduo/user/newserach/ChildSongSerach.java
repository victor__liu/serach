package com.miduo.user.newserach;

import java.util.ArrayList;

import com.miduo.user.newserach.entity.Album;

//儿歌 完成
public class ChildSongSerach extends Search {
	public static void main(String[] args) {
		ChildSongSerach css = new ChildSongSerach();
		int tag = 7;
		css.respectiveParser(tag);
	}
	@Override
	public void respectiveParser(int tag) {
		String[] special = new String[] { "- 网趣童谣", "2-3岁", "1-2岁", "4-5岁",
				"(hot)", "- 网趣儿歌", "- 网趣语文", "小蛮头听儿歌", "- 网趣新儿歌", "亲子系列-" };
		int i = 1;
		while (true) {
			ArrayList<Album> albums = searchAlbum("http://lebo.baidu.com/tag/儿歌?start="
					+ i + "&limit=20&sort=hot_desc&fr=frame");
			ArrayList<Album> finalAlbums = new ArrayList<>();
			if (albums.size() != 0) {
				for (int j = 0; j < albums.size(); j++) {
					for (String s : special) {
						albums.get(j).setName(
								albums.get(j).getName().replace(s, ""));
					}
					if (albums.get(j).getName().contains("中文")
							|| albums.get(j).getName().contains("英文")
							|| albums.get(j).getName().contains("字母")) {
						albums.get(j).setCategory_id(6);
						System.out.println("进入了6 " + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else if (albums.get(j).getName().contains("诗")
							|| albums.get(j).getName().contains("国学")
							|| albums.get(j).getName().contains("三字经")) {
						albums.get(j).setCategory_id(3);
						System.out.println("进入了3 " + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else {
						albums.get(j).setCategory_id(tag);
						System.out.println("儿童故事的其他数据==》经典童谣         "
								+ albums.get(j).getName() + " " + j);
						finalAlbums.add(albums.get(j));
					}
				}
				i = i + 20;
				update(finalAlbums);
			} else {
				System.out.println("查询完毕  儿歌");
				break;
			}
		}
	}
}
