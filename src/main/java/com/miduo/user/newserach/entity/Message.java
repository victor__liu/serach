package com.miduo.user.newserach.entity;

public class Message {
	
	private String songId;
	private String songName;
	private String djName;
    private String pic;
    private String albumPic;
    
	public String getSongId() {
		return songId;
	}
	public void setSongId(String songId) {
		this.songId = songId;
	}
	public String getSongName() {
		return songName;
	}
	public void setSongName(String songName) {
		this.songName = songName;
	}
	public String getDjName() {
		return djName;
	}
	public void setDjName(String djName) {
		this.djName = djName;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getAlbumPic() {
		return albumPic;
	}
	public void setAlbumPic(String albumPic) {
		this.albumPic = albumPic;
	}
	@Override
	public String toString() {
		return "Message [songId=" + songId + ", songName=" + songName
				+ ", djName=" + djName + ", pic=" + pic + ", albumPic="
				+ albumPic + "]";
	}
    
}
