package com.miduo.user.newserach.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;




@Entity
@Table(name="album")
public class Album {

	private int album_id=0;
	private String name;
	private String author;
	private String description;
	private String img;
	private String age = "3|4|5|6";
	private byte priority = 0;
	private byte visable = 1;
	private int counter =0;
	private int lebo_id;
	private String tags=null;
	private int category_id;
	private List<Lebo> lebo;
	
	@OneToMany(mappedBy = "album", cascade = {CascadeType.ALL})
	public List<Lebo> getlebo() {
		return lebo;
	}
	public void setlebo(List<Lebo> lebo) {
		this.lebo = lebo;
	}
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getAlbum_id() {
		return album_id;
	}
	public void setAlbum_id(int album_id) {
		this.album_id = album_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	@Column(columnDefinition = "bit")
	public byte getPriority() {
		return priority;
	}

	public void setPriority(byte priority) {
		this.priority = priority;
	}
	@Column(columnDefinition = "bit")
	public byte getVisable() {
		return visable;
	}
	public void setVisable(byte visable) {
		this.visable = visable;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public int getLebo_id() {
		return lebo_id;
	}
	
	public void setLebo_id(int lebo_id) {
		this.lebo_id = lebo_id;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	@Override
	public String toString() {
		return "Album [album_id=" + album_id + ", name=" + name + ", author="
				+ author + ", description=" + description + ", img=" + img
				+ ", age=" + age + ", priority=" + priority + ", visable="
				+ visable + ", counter=" + counter + ", lebo_id=" + lebo_id
				+ ", tags=" + tags + ", category_id=" + category_id + "]";
	}
	
}
