package com.miduo.user.newserach;

import java.util.ArrayList;

import com.miduo.user.newserach.entity.Album;

//儿童英语 完成
public class ChildEngilshSerach extends Search {
	public static void main(String[] args) {
		ChildEngilshSerach css = new ChildEngilshSerach();
		int tag = 7;
		css.respectiveParser(tag);
	}

	@Override
	public void respectiveParser(int tag) {
		String[] special = new String[] { "宝宝5岁", "汪培珽", "英语启蒙动画片", "(Audio)",
				"1 ， 2。 ", "（64" };

		int i = 1;
		while (true) {

			ArrayList<Album> albums =searchAlbum("http://lebo.baidu.com/tag/儿童英语?start=" + i
							+ "&limit=20&sort=hot_desc&fr=frame");
			ArrayList<Album> finalAlbums = new ArrayList<>();
			
			if (albums.size() != 0) {
				for (int j = 0; j < albums.size(); j++) {
					if (albums.get(j).getName().contains("廖彩杏")) {
						System.out.println("数据已删除       "
								+ albums.get(j).getName() + j);
						continue;
					}
					for (String s : special) {
						albums.get(j).setName(
								albums.get(j).getName().replace(s, ""));
					}
					if (albums.get(j).getName().contains("绘本")
							|| albums.get(j).getName().contains("故事")) {
						albums.get(j).setCategory_id(2);
						System.out.println("进入了2 " + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else {
						albums.get(j).setCategory_id(tag);
						System.out.println("儿童英语的其他数据==》儿童英语         "
								+ albums.get(j).getName() + " " + j);
						finalAlbums.add(albums.get(j));
					}
				}
				i = i + 20;
				update(finalAlbums);
			} else {
				System.out.println("查询完毕  儿童英语");
				break;
			}
		}
	}
}
