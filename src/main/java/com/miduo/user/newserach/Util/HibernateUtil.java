package com.miduo.user.newserach.Util;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
public class HibernateUtil {
    private static Configuration configuration = null;
   private static SessionFactory sessionFactory = null;
    private static ServiceRegistry serviceRegistry = null;
    
    static {
        // 加载Hibernate主配置文件
    	 configuration = new Configuration().configure();
    	  serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
    	  sessionFactory = configuration.buildSessionFactory(serviceRegistry);
       
    }
    /**
     * 创建session
     */
    public static Session getSession() {
       

    	System.out.println(sessionFactory);
        return sessionFactory.openSession();
    }
    
    public static void main(String[] args) {
        System.out.println(getSession());
    }
}
