package com.miduo.user.newserach;

import java.util.ArrayList;

import com.miduo.user.newserach.entity.Album;

//儿童百科 完成
public class ChildKnowledgeSerach extends Search {
	public static void main(String[] args) {
		ChildKnowledgeSerach css = new ChildKnowledgeSerach();
		int tag = 5;
		css.respectiveParser(tag);
	}

	@Override
	public void respectiveParser(int tag) {
		String[] special = new String[] { "【3岁+】 ", "【4岁+】 ",
				"-风靡韩国的百万畅销科学漫画书", " - 国学宝 -小孔子", "(1-108期)05", "(1-108期)04",
				"(1-108期)01", "（0-6岁） ", "（3岁以上） ", "三月" };
		int i = 1;
		while (true) {
			ArrayList<Album> albums =searchAlbum("http://lebo.baidu.com/tag/儿童百科?start=" + i
							+ "&limit=20&sort=hot_desc&fr=frame");
			ArrayList<Album> finalAlbums = new ArrayList<>();
			if (albums.size() != 0) {

				for (int j = 0; j < albums.size(); j++) {
					for (String s : special) {
						albums.get(j).setName(
								albums.get(j).getName().replace(s, ""));
					}
					if (albums.get(j).getName().contains("成语")) {
						albums.get(j).setCategory_id(1);
						System.out.println("进入了1 " + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else if (albums.get(j).getName().contains("诗")
							|| albums.get(j).getName().contains("国学")
							|| albums.get(j).getName().contains("论语")
							|| albums.get(j).getName().contains("百家姓")) {
						albums.get(j).setCategory_id(3);
						System.out.println("进入了3 " + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else {
						albums.get(j).setCategory_id(tag);
						System.out.println("儿童百科的其他数据==》儿童百科         "
								+ albums.get(j).getName() + " " + j);
						finalAlbums.add(albums.get(j));
					}
				}
				i = i + 20;
				update(finalAlbums);
			} else {
				System.out.println("查询完毕   儿童百科");
				break;
			}
		}
	}
}
