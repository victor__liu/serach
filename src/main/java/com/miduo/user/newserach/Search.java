package com.miduo.user.newserach;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.AndFilter;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.HasChildFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.util.NodeList;

import com.google.gson.Gson;
import com.miduo.user.newserach.Util.HibernateUtil;
import com.miduo.user.newserach.entity.Album;
import com.miduo.user.newserach.entity.Lebo;
import com.miduo.user.newserach.entity.Message;

public abstract class Search  {
	private static Logger logger = Logger.getLogger(Search.class);
	public abstract void respectiveParser(int tag);
	public void update(ArrayList<Album> albums) {
		for (int j = 0; j < albums.size(); j++) {
			Session session = HibernateUtil.getSession();
			// 查询要修改的数据
			// Album album = new Album();
			// 开启事务
			Transaction transactions = session.beginTransaction();
			try {
				System.out.println("开始了");
				List<Lebo> lebolist =searchLebo(albums.get(j));
				albums.get(j).setlebo(lebolist);
				session.save(albums.get(j));
				transactions.commit();
				logger.info("一条数据已经插入完毕" + albums.get(j).getName() + "  " + j);
			} catch (HibernateException e) {
				e.printStackTrace();
				// 回滚事务
				transactions.rollback();
			} finally {
				// 关闭连接
				session.close();
			}
		}
	}

	// 查询分页内容的方法
	public  List<Lebo> searchLebo(Album album) {
		List<Lebo> lebolist = new ArrayList<Lebo>();
		try {
			int j = 0;
			do {
				String newurl = "http://lebo.baidu.com/album/"
						+ album.getLebo_id() + "?start=" + j
						+ "&limit=20&sort=albumno_desc&fr=frame";

				Parser parser = new Parser(
						(HttpURLConnection) (new URL(newurl)).openConnection());

				parser.setEncoding("utf-8");
				NodeFilter filter1 = new HasAttributeFilter("li");
				NodeFilter innerFilter = new TagNameFilter("span");
				NodeFilter filter2 = new HasChildFilter(innerFilter);
				NodeFilter filter3 = new AndFilter(filter1, filter2);
				NodeList nodes = parser.extractAllNodesThatMatch(filter3);
				if (nodes.size() != 0) {
					for (int k = 0; k < nodes.size(); k++) {
						int num1 = nodes.elementAt(k).toHtml().indexOf("{");
						String sub1 = nodes.elementAt(k).toHtml()
								.substring(num1);
						int num = sub1.indexOf("}");
						String sub2 = sub1.substring(0, num + 1);
						String sub3 = sub2.replace("&quot", "\"");
						String sub4 = sub3.replace(";", "");
						Message age = new Gson().fromJson(sub4, Message.class);
						Lebo lebo = new Lebo();
						lebo.setTrack_id(Integer.parseInt(age.getSongId()));
						lebo.setName(age.getSongName());
						lebo.setAuthor(age.getDjName());
						lebo.setAlbum(album);
						lebolist.add(lebo);
						// logger.info(lebo.toString());
						// System.out
						// .println("=================================================");
					}
					j = j + 20;
				} else {
					logger.info("没了");
					break;
				}
			} while (true);
			logger.info("以上是新title");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lebolist;
	}
	// 查询主页面数据程序
	public ArrayList<Album> searchAlbum(String url) {
		// 返回的List
		ArrayList<Album> albums = new ArrayList<>();
		String[] id = searchId(url);
		try {
			if (id != null) {
				for (String sid : id) {
					Album album = new Album();
					Parser parser = new Parser((HttpURLConnection) (new URL(
							"http://lebo.baidu.com/album/" + sid
									+ "?fr=frame")).openConnection());
					parser.setEncoding("utf-8");
					NodeFilter filter = new HasAttributeFilter("class",
							"album-info-wrapper");
					NodeList nodes = parser.extractAllNodesThatMatch(filter);
					Node textnode = (Node) nodes.elementAt(0);
					Parser parserTitle = new Parser(textnode.toHtml());
					Parser parserImg = new Parser(textnode.toHtml());
					Parser parserAuthor = new Parser(textnode.toHtml());
					Parser parserDes = new Parser(textnode.toHtml());
					NodeFilter filterTitle = new HasAttributeFilter("class",
							"ellipsis");
					NodeFilter filterImg = new HasAttributeFilter("class",
							"album-pic");
					NodeFilter filterAuthor = new HasAttributeFilter("class",
							"dj-name");
					NodeFilter filterDes = new HasAttributeFilter("class",
							"des");
					NodeList nodesTitle = parserTitle
							.extractAllNodesThatMatch(filterTitle);
					NodeList nodesImg = parserImg
							.extractAllNodesThatMatch(filterImg);
					NodeList nodesAuthor = parserAuthor
							.extractAllNodesThatMatch(filterAuthor);
					NodeList nodesDes = parserDes
							.extractAllNodesThatMatch(filterDes);
					Node textnodeTitle = (Node) nodesTitle.elementAt(0);
					Node textnodeImg = (Node) nodesImg.elementAt(0);
					String src1 = textnodeImg.toHtml().substring(28);
					int num = src1.indexOf("\"");
					String src2 = src1.substring(0, num);
					Node textnodeAuthor = (Node) nodesAuthor.elementAt(0);
					Node textnodeDes = (Node) nodesDes.elementAt(0);
					System.out.println(textnodeTitle.toPlainTextString());
					album.setName(textnodeTitle.toPlainTextString());
					System.out.println(src2);
					album.setImg(src2);
					System.out.println(textnodeAuthor.toPlainTextString());
					album.setAuthor(textnodeAuthor.toPlainTextString());
					System.out.println(textnodeDes.toPlainTextString());
					album.setDescription(textnodeDes.toPlainTextString());
					int lebo_id = Integer.parseInt(sid);
					album.setLebo_id(lebo_id);
					albums.add(album);
					System.out
							.println("=================================================");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return albums;
	}

	// 查询id
	String[] searchId(String url) {
		ArrayList<String> id = new ArrayList<>();
		try {
			Parser parser = new Parser(
					(HttpURLConnection) (new URL(url)).openConnection());
			parser.setEncoding("utf-8");
			NodeFilter filter = new HasAttributeFilter("class", "play");
			NodeList nodes = parser.extractAllNodesThatMatch(filter);
			if (nodes != null) {
				for (int i = 0; i < nodes.size(); i++) {
					Node textnode = (Node) nodes.elementAt(i);
					String id1 = textnode.toHtml().substring(52);
					int num = id1.indexOf("\"");
					String id2 = id1.substring(0, num);
					id.add(id2);
					// logger.info(id2);
					// System.out
					// .println("=================================================");
				}
				logger.info("以上是ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String[] array = id.toArray(new String[0]);
		return array;
	}
}