package com.miduo.user.newserach;

import java.util.ArrayList;

import com.miduo.user.newserach.entity.Album;

//儿童故事
public class ChildStorySerach extends Search {
	public static void main(String[] args) {
		ChildStorySerach cecs = new ChildStorySerach();
		// 其余的放的tag
		int tag = 2;
		cecs.respectiveParser(tag);
	}

	@Override
	public void respectiveParser(int tag) {
		int i = 1;
		String[] special = new String[] { "心贝电台——", "橙心亲子-", "小蛮头听故事", "聪明宝宝—",
				"——智慧森林", "呼噜博士讲故事" };
		while (true) {
			ArrayList<Album> albums = searchAlbum("http://lebo.baidu.com/tag/儿童故事?start="
					+ i + "&limit=20&sort=hot_desc&fr=frame");
			ArrayList<Album> finalAlbums = new ArrayList<>();
			if (albums.size() != 0) {
				for (int j = 0; j < albums.size(); j++) {
					// 特殊处理判断是否含有特殊字符并去除
					for (String s : special) {
						albums.get(j).setName(
								albums.get(j).getName().replace(s, ""));
					}
					albums.get(j).setName(
							albums.get(j).getName().replace("【", ""));
					albums.get(j).setName(
							albums.get(j).getName().replace("】", ""));
					if (albums.get(j).getName() == "鞠萍姐姐講故事") {
						albums.get(j).setName("鞠萍姐姐讲故事");
					}
					if (albums.get(j).getName() == "【爱丽丝梦游奇境】可乐姐姐版") {
						albums.get(j).setName("爱丽丝梦游奇境");
					}
					if (albums.get(j).getName() == "手机录音") {
						continue;
					}
					albums.get(j).setName(
							albums.get(j).getName()
									.replaceAll("^\\-*|\\-*$", "")
									.replaceAll("[\\(（][\\w\\W]*[\\)）]", ""));
					if (albums.get(j).getName().contains("童话")) {
						albums.get(j).setCategory_id(1);
						System.out.println("进入了1" + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else if (albums.get(j).getName().contains("诗")
							|| albums.get(j).getName().contains("国学")
							|| albums.get(j).getName().contains("三字经")) {
						albums.get(j).setCategory_id(3);
						System.out.println("进入了3" + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else if (albums.get(j).getName().contains("童话")
							|| albums.get(j).getName().contains("寓言")
							|| albums.get(j).getName().contains("成语")) {
						albums.get(j).setCategory_id(1);
						System.out.println("进入了1" + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));

					} else if (albums.get(j).getName().contains("中文")
							|| albums.get(j).getName().contains("英文")
							|| albums.get(j).getName().contains("英语")) {
						albums.get(j).setCategory_id(6);
						System.out.println("进入了6" + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else if (albums.get(j).getName().contains("歌曲")
							|| albums.get(j).getName().contains("音乐")
							|| albums.get(j).getName().contains("民乐")) {
						albums.get(j).setCategory_id(7);
						System.out.println("进入了7" + albums.get(j).getName()
								+ " " + j);
						finalAlbums.add(albums.get(j));
					} else {
						albums.get(j).setCategory_id(tag);
						System.out.println("儿童故事的其他数据==》经典故事         "
								+ albums.get(j).getName() + " " + j);
						finalAlbums.add(albums.get(j));
					}
				}
				update(finalAlbums);
				i = i + 20;
			} else {
				System.out.println("查询完毕 儿童故事");
				break;
			}

		}
	}

}
